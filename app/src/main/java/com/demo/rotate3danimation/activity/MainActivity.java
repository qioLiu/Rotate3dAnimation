package com.demo.rotate3danimation.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.demo.rotate3danimation.R;
import com.demo.rotate3danimation.animation.Rotate3dAnimation;

public class MainActivity extends AppCompatActivity {

    private TextView tvName;
    private Rotate3dAnimation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    public void initView(){
        tvName = (TextView) findViewById(R.id.tvName);
        float centerX = tvName.getWidth() / 2;
        float centerY = tvName.getHeight() / 2;
        animation = new Rotate3dAnimation(centerX, centerY, 50,  0, 60);
        animation.setDuration(3000);
        animation.setFillAfter(true);
        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvName.startAnimation(animation);
            }
        });

    }
}
