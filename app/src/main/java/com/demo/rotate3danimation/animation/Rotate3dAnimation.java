package com.demo.rotate3danimation.animation;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by six.sev on 2017/3/23.
 */

public class Rotate3dAnimation extends Animation {

    private float mCenterX;
    private float mCenterY;
    private float mDepthZ;
    private float mFromDegree;
    private float mToDegree;
    private Camera mCamera;

    public Rotate3dAnimation(float centerX, float centerY, float depthZ, float fromDegree, float toDegree){
        super();
        this.mCenterX = centerX;
        this.mCenterY = centerY;
        this.mDepthZ = depthZ;
        this.mFromDegree = fromDegree;
        this.mToDegree = toDegree;
        mCamera = new Camera();
    }

    /**
     *
     * @param interpolatedTime 旋转的进度。
     * @param t
     */
    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        Matrix matrix = t.getMatrix();
        mCamera.save();

        float degree = mFromDegree + (mToDegree - mFromDegree) * interpolatedTime;
        // z的偏移会越来越小。这就会形成这样一个效果，我们的View从一个很远的地方向我们移过来，越来越近，最终移到了我们的窗口上面～
        mCamera.translate(0.0f, 0.0f, mDepthZ * (1.0f - interpolatedTime));
        //z的偏移会越来越大。这就会形成这样一个效果，越来越远，最终移到了我们的窗口上面～
        // mCamera.translate(0.0f, 0.0f, mDepthZ * interpolatedTime);

        // 是给我们的View加上旋转效果，在移动的过程中，视图还会移Y轴为中心进行旋转。
        mCamera.rotateY(degree);

        // 这个是将我们刚才定义的一系列变换应用到变换矩阵上面，调用完这句之后，我们就可以将camera的位置恢复了，以便下一次再使用。
        mCamera.getMatrix(matrix);
        mCamera.restore();

        // 以View的中心点为旋转中心,如果不加这两句，就是以（0,0）点为旋转中心
        matrix.preTranslate(-mCenterX, -mCenterY);
        matrix.postTranslate(mCenterX, mCenterY);
    }
}
